require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const compression = require('compression')
//const bodyParser = require('body-parser')
//const cors = require('cors')

const couchUri = process.env.COUCH_URI

console.log('Starting server...')

// The REST service:
const app = express()

// compress all responses
app.use(compression())

//app.use(morgan('tiny'))
app.use(morgan((tokens, req, res) => {
	return [
		tokens.method(req, res),
		tokens.url(req, res),
		tokens.status(req, res),
		tokens.res(req, res, 'content-length'), '-',
		tokens['response-time'](req, res), 'ms',
		JSON.stringify(req.body)
	].join(' ')
}))

//app.use(cors())

// Redirect http to https
app.use((req, res, next) => {
	if (req.header('x-forwarded-proto') && req.header('x-forwarded-proto') !== 'https') {
		const newLocation = `https://${req.header('host')}${req.url}`
		console.debug('redirecting to: ', newLocation)
		res.redirect(newLocation)
	} else {
		next()
	}
})

app.use(express.static('build'))
//app.use(bodyParser.json())

// URLs =======================================================================
app.get('/db/couch', (req, res) => {
	res.send({ couchUri: couchUri })
})

app.use(express.static('public'))
// Auf ungültige URLs reagieren
const unknownEndpoint = (request, response) => {
	response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

// Fehler an client senden
const errorHandler = (error, request, response, next) => {
	console.error('Error: ', error.message)

	if (error.name === 'CastError' && error.kind === 'ObjectId') {
		return response.status(400).send({ error: 'malformatted id' })
	}
	next(error)
}

app.use(errorHandler)

const PORT = process.env.PORT || 3002
app.listen(PORT, () => {
	console.log(`Server running on port ${PORT}`)
})
